import React, { useEffect } from "react";
import { AppProps } from "next/app";
import "@styles/global.css";
import { Provider } from "react-redux";
import store, { persistor } from "@redux/store";
import { PersistGate } from "redux-persist/integration/react";
import AppLayout from "@components/Layouts/AppLayout";
import theme from "@src/theme";
// react slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import { CacheProvider, EmotionCache } from "@emotion/react";
import createEmotionCache from "src/theme/createEmotionCache";

const clientSideEmotionCache = createEmotionCache();
interface MyAppProps extends AppProps {
    emotionCache?: EmotionCache;
}

function MyApp({
    Component,
    emotionCache = clientSideEmotionCache,
    pageProps,
}: MyAppProps): JSX.Element {
    return (
        <CacheProvider value={emotionCache}>
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline />
                        <AppLayout>
                            <Component {...pageProps} />
                        </AppLayout>
                    </ThemeProvider>
                </PersistGate>
            </Provider>
        </CacheProvider>
    );
}

export default MyApp;
