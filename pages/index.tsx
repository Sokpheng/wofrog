import Layout from "@components/Layouts/Layout";
import React from "react";
import {
    CTASection,
    HeroSection,
    IntroSection,
    ProjectSection,
    TeamSection,
} from "@src/modules/Home";
import Link from "next/link";
import { useSelector } from "react-redux";
import { RootState } from "@src/redux/reducers";
import Service from "@src/config/Service";
import { NextPage } from "next";
import _ from "lodash";
import { Stack } from "@mui/material";

const HomePage: NextPage<any> = ({ data }) => {
    const { HomeCallToAction, HomeHero, HomeIntro, HomeProjects, HomeTeam } =
        data;

    console.log("resp: ", data);

    if (_.isEmpty(data)) {
        return null;
    }
    return (
        <div>
            <Layout title="Home Page">
                <Stack direction="column">
                    <HeroSection data={HomeHero} />
                    <IntroSection data={HomeIntro} />
                    <ProjectSection data={HomeProjects} />
                    <TeamSection data={HomeTeam} />
                    <CTASection data={HomeCallToAction} />
                </Stack>
            </Layout>
        </div>
    );
};

export default HomePage;

export async function getStaticProps() {
    const resp = await Service.find("api/home-page");

    return {
        props: { data: resp.data?.data?.attributes },
    };
}
