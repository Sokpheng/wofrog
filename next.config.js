const withPlugins = require("next-compose-plugins");

module.exports = withPlugins([], {
    reactStrictMode: true,
    env: {
        API_URL: process.env.APP_KEY,
        // PORT: 1337,
    },
    images: {
        domains: ['localhost', 'wofrog.herokuapp.com'],
    },
});
