import CardProject from "@components/Cards/CardProject";
import { Icon } from "@iconify/react";
import {
    Box,
    Button,
    Card,
    CardContent,
    CardMedia,
    Container,
    Grid,
    Stack,
    Typography,
    useMediaQuery,
    useTheme,
} from "@mui/material";
import Image from "next/image";
import React, { FC } from "react";
import { HomeSectionStyles } from "./styles";

export const ProjectSection = ({ data }: { data: any }) => {
    // console.log("ProjectSection data", data);

    const theme = useTheme();
    const isMatch = useMediaQuery(theme.breakpoints.up("md"));

    return (
        <HomeSectionStyles
            id="projects"
            imageUrl={
                isMatch ? data?.BackgroundImage?.data?.attributes?.url : ""
            }
        >
            <Container maxWidth="xl">
                <Stack spacing={5}>
                    <Stack
                        flexGrow={1}
                        px={4}
                        alignItems={isMatch ? "flex-start" : "center"}
                        spacing={2}
                        sx={{
                            color: isMatch
                                ? (theme) => theme.palette.common.white
                                : (theme) => theme.palette.primary.main,
                        }}
                    >
                        <Icon icon={data?.SectionHeading?.Icon} fontSize={64} />
                        <Typography
                            variant="h5"
                            component="h2"
                            fontWeight="bold"
                        >
                            {data?.SectionHeading?.Heading}
                        </Typography>
                        <Typography
                            variant="body1"
                            component="p"
                            maxWidth="80ch"
                        >
                            {data?.SectionHeading?.SubHeading}
                        </Typography>
                    </Stack>
                    <Stack>
                        <Grid container justifyContent="flex-start" spacing={3}>
                            {data?.Projects?.data &&
                                data.Projects.data.map((project: any) => (
                                    <Grid
                                        item
                                        xs={12}
                                        sm={6}
                                        md={4}
                                        lg={3}
                                        key={project?.id}
                                    >
                                        <CardProject
                                            image={
                                                project?.attributes?.Images
                                                    ?.data[0]?.attributes?.url
                                            }
                                            heading={project?.attributes?.Name}
                                            description={
                                                project?.attributes?.Description
                                            }
                                            callToAction={
                                                project?.attributes
                                                    ?.CallToAction
                                            }
                                        />
                                    </Grid>
                                ))}
                        </Grid>
                    </Stack>
                </Stack>
            </Container>
        </HomeSectionStyles>
    );
};
