import {
    Box,
    Button,
    Card,
    CardContent,
    CardMedia,
    Container,
    IconButton,
    Stack,
    Typography,
} from "@mui/material";
import React, { FC } from "react";
import SectionHeading from "./SectionHeading";
import { HomeSectionStyles } from "./styles";
import Slider from "react-slick";
import { Icon } from "@iconify/react";
import Image from "next/image";
import CardTeamMember from "@components/Cards/CardTeamMember";

export const TeamSection = ({ data }: { data: any }) => {
    // console.log("team: ", data);
    var settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: true,
        initialSlide: 2,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    // infinite: true,
                    dots: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    return (
        <HomeSectionStyles id="teams" >
            <Container maxWidth="xl">
                <Stack spacing={5}>
                    <SectionHeading
                        icon={data?.SectionHeading?.Icon}
                        heading={data?.SectionHeading?.Heading}
                        subHeading={data?.SectionHeading?.SubHeading}
                    />
                    <Stack
                        sx={{
                            "& .slick-track": {
                                display: "flex",
                                justifyContent: "center",
                            },
                        }}
                    >
                        <Slider
                            prevArrow={<></>}
                            nextArrow={<></>}
                            {...settings}
                        >
                            {data?.TeamMember?.data &&
                                data.TeamMember.data.map((team: any) => (
                                    <Stack key={team.id} px={2}>
                                        <CardTeamMember
                                            name={team?.attributes?.Name}
                                            image={
                                                team?.attributes?.Images
                                                    ?.data[0]?.attributes?.url
                                            }
                                            description={
                                                team?.attributes?.Description
                                            }
                                            callToAction={
                                                team?.attributes?.CallToAction
                                            }
                                        />
                                    </Stack>
                                ))}
                        </Slider>
                    </Stack>
                </Stack>
            </Container>
        </HomeSectionStyles>
    );
};
