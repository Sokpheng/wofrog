import { Box, BoxProps } from "@mui/material";
import { styled, alpha } from "@mui/material/styles";
import { api } from "@src/config";

interface BoxPropStyles extends BoxProps {
    imageUrl?: string;
}

export const HomeHeroStyles = styled(Box)<BoxPropStyles>(
    ({ theme, imageUrl }) => ({
        height: "100vh",
        backgroundImage: `linear-gradient(to top, #1976D2, #FF5252)`,
        ...(imageUrl && {
            [theme.breakpoints.up("md")]: {
                backgroundImage: `url(${api.apiUrl + imageUrl})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "70% 100%",
                backgroundPosition: "bottom  right",
            },
        }),
        marginTop: -64,
    }),
);

export const HomeSectionStyles = styled(Box)<BoxPropStyles>(
    ({ theme, imageUrl }) => ({
        paddingTop: 120,
        paddingBottom: 120,
        [theme.breakpoints.down("md")]: {
            paddingTop: 64,
            paddingBottom: 64,
        },
        [theme.breakpoints.down("sm")]: {
            paddingTop: 48,
            paddingBottom: 48,
        },
        [theme.breakpoints.down("xs")]: {
            paddingTop: 32,
            paddingBottom: 32,
        },
        ...(imageUrl && {
            backgroundImage: `url(${api.apiUrl + imageUrl})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "100% 100%",
        }),
    }),
);

export const HomeCTAStyles = styled(HomeSectionStyles)<BoxPropStyles>(
    ({ theme, imageUrl }) => ({
        ...(imageUrl && {
            backgroundImage: `url(${api.apiUrl + imageUrl})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "100% 100%",
            backgroundPosition: "center bottom",
        }),
    }),
);

export const CTABoxStyles = styled(Box)(({ theme }) => ({
    flexGrow: 1,
    backgroundColor: "red",
    padding: theme.spacing(5),
    borderRadius: 16,
    backgroundImage: `linear-gradient(to top, #1976D2, #FF5252)`,
    color: theme.palette.common.white,
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
        padding: theme.spacing(3),
    },
    [theme.breakpoints.down("xs")]: {
        padding: theme.spacing(2),
    },
}));
