import { Box, Button, Container, Link, Stack, Typography } from "@mui/material";
import React, { FC } from "react";
import { CTABoxStyles, HomeCTAStyles } from "./styles";
import NextLink from "next/link";
import { Icon } from "@iconify/react";

export const CTASection = ({ data }: { data: any }) => {
    console.log("CATsection data", data);
    return (
        <HomeCTAStyles
            imageUrl={data?.BackgroundImage?.data?.attributes?.url}
            bgcolor={(theme) => theme.palette.grey[50]}
        >
            <Container maxWidth="sm">
                <CTABoxStyles>
                    <Stack spacing={5}>
                        <Typography variant="h5">{data?.Heading}</Typography>
                        <Box>
                            <NextLink
                                href={
                                    data?.CallToAction?.Menu?.data?.attributes
                                        ?.Path || "/"
                                }
                                passHref
                            >
                                <Link underline="none">
                                    <Button
                                        variant="contained"
                                        color="inherit"
                                        sx={{
                                            color: (theme) =>
                                                theme.palette.error.main,
                                        }}
                                        startIcon={
                                            <Icon
                                                icon={
                                                    data?.CallToAction?.Icon ||
                                                    ""
                                                }
                                            />
                                        }
                                    >
                                        {data?.CallToAction?.Label}
                                    </Button>
                                </Link>
                            </NextLink>
                        </Box>
                    </Stack>
                </CTABoxStyles>
            </Container>
        </HomeCTAStyles>
    );
};
