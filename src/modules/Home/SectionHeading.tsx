import { Icon } from "@iconify/react";
import { Stack, Typography } from "@mui/material";
import React from "react";

type Props = {
    icon?: string;
    heading?: string;
    subHeading?: string;
};

const SectionHeading = ({ icon, heading, subHeading }: Props) => {
    return (
        <Stack
            flexGrow={1}
            alignItems="center"
            spacing={2}
            sx={{ color: (theme) => theme.palette.primary.main }}
        >
            <Icon icon={icon || ""} fontSize={64} />
            <Typography variant="h5" component="h2" fontWeight="bold">
                {heading}
            </Typography>
            <Typography
                variant="body1"
                component="p"
                maxWidth="80ch"
                textAlign="center"
            >
                {subHeading}
            </Typography>
        </Stack>
    );
};

export default SectionHeading;
