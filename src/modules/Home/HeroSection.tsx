import { Icon } from "@iconify/react";
import {
    Box,
    Button,
    Container,
    Link,
    Stack,
    Typography,
    useMediaQuery,
    useTheme,
} from "@mui/material";
import { api } from "@src/config";
import Image from "next/image";
import React, { FC } from "react";
import NextLink from "next/link";
import { HomeHeroStyles } from "./styles";

export const HeroSection = ({ data }: { data: any }) => {
    // console.log("dataaaa hero: ", data);
    const theme = useTheme();

    const isMatch = useMediaQuery(theme.breakpoints.up("md"));

    return (
        <HomeHeroStyles imageUrl={data?.BackgroundImage?.data?.attributes?.url}>
            <Container maxWidth="xl" sx={{ height: "100%", p: 0 }}>
                <Stack
                    flexGrow={1}
                    direction="row"
                    justifyContent="center"
                    height="100%"
                    alignItems="center"
                >
                    <Stack
                        flex={1}
                        direction="column"
                        spacing={5}
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Typography
                            variant="h4"
                            fontWeight="bold"
                            component="h1"
                            color="primary"
                        >
                            {data?.Heading}
                        </Typography>
                        <Typography
                            variant="body1"
                            sx={{
                                opacity: 0.8,
                                maxWidth: "80ch",
                                textAlign: "center",
                            }}
                            component="h2"
                            color="primary"
                        >
                            {data?.SubHeading}
                        </Typography>
                        <Box>
                            <NextLink
                                href={
                                    data?.CallToAction?.Menu?.data?.attributes
                                        ?.Path || "/"
                                }
                                passHref
                            >
                                <Link underline="none">
                                    <Button
                                        variant="contained"
                                        color="error"
                                        startIcon={
                                            <Icon
                                                icon={
                                                    data?.CallToAction?.Icon ||
                                                    ""
                                                }
                                            />
                                        }
                                    >
                                        {data?.CallToAction?.Label}
                                    </Button>
                                </Link>
                            </NextLink>
                        </Box>
                    </Stack>
                    {isMatch && (
                        <Stack
                            flex={1}
                            justifyContent="center"
                            alignItems="center"
                        >
                            <Box
                                sx={{
                                    position: "relative",
                                    aspectRatio: "1 / 1",
                                    width: "70%",
                                }}
                            >
                                <Image
                                    src={
                                        api.apiUrl +
                                        data?.HeroImage?.data?.attributes?.url
                                    }
                                    priority
                                    layout="fill"
                                    objectFit="contain"
                                    alt="hero"
                                />
                            </Box>
                        </Stack>
                    )}
                </Stack>
            </Container>
        </HomeHeroStyles>
    );
};
