import { CardIntro } from "@components/Cards";
import { Icon } from "@iconify/react";
import {
    Card,
    CardContent,
    CardMedia,
    Container,
    Grid,
    Stack,
    Typography,
} from "@mui/material";
import _ from "lodash";
import Image from "next/image";
import React, { FC } from "react";
import SectionHeading from "./SectionHeading";
import { HomeSectionStyles } from "./styles";

export const IntroSection = ({ data }: { data: any }) => {
    // console.log("intro: ", data);

    if (_.isEmpty(data)) return null;

    return (
        <Container maxWidth="xl">
            <HomeSectionStyles>
                <Stack spacing={5}>
                    <SectionHeading
                        icon={data?.SectionHeading?.Icon}
                        heading={data?.SectionHeading?.Heading}
                        subHeading={data?.SectionHeading?.SubHeading}
                    />
                    <Stack>
                        <Grid container spacing={3} justifyContent="center">
                            {data?.HomeIntroCard &&
                                data.HomeIntroCard.map((intro: any) => (
                                    <Grid
                                        item
                                        xs={12}
                                        sm={6}
                                        md={4}
                                        lg={3}
                                        key={intro.id}
                                    >
                                        <CardIntro
                                            image={
                                                intro?.Cover?.data?.attributes
                                                    ?.url
                                            }
                                            heading={intro?.Title}
                                            description={intro.Description}
                                        />
                                    </Grid>
                                ))}
                        </Grid>
                    </Stack>
                </Stack>
            </HomeSectionStyles>
        </Container>
    );
};
