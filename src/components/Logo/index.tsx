import React from "react";
import NextLink from "next/link";
import { Box, Link } from "@mui/material";
import Image from "next/image";
import { api } from "@src/config";

type Props = {
    logo: any;
    width?: number | string;
};

const Logo = ({ logo, width = 136 }: Props) => {
    return (
        <NextLink href="/" passHref>
            <Link>
                <Box
                    sx={{
                        position: "relative",
                        aspectRatio: "17 / 7",
                        width,
                    }}
                >
                    <Image
                        src={api.apiUrl + logo?.data?.attributes?.url}
                        layout="fill"
                        objectFit="contain"
                        alt="logo"
                    />
                </Box>
            </Link>
        </NextLink>
    );
};

export default Logo;
