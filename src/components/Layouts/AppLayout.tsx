import { api } from "@src/config";
import Service from "@src/config/Service";
import { RootState } from "@src/redux/reducers";
import { getSettings } from "@src/redux/slices/settings";
import Head from "next/head";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

type Props = {
    children: React.ReactNode;
};

const AppLayout = ({ children }: Props) => {
    const dispatch = useDispatch();
    const { data } = useSelector((state: RootState) => state.settings);
    console.log("man");
    useEffect(() => {
        dispatch(getSettings());
    }, []);

    return (
        <>
            {data && (
                <Head>
                    {data?.SiteName && <title>{data.SiteName}</title>}
                    {data?.Favicon?.data && (
                        <link
                            rel="icon"
                            href={
                                api.apiUrl + data.Favicon.data?.attributes?.url
                            }
                        />
                    )}
                    {data?.description && (
                        <meta name="description" content={data.description} />
                    )}
                </Head>
            )}
            {children}
        </>
    );
};

export default AppLayout;
