import { Footer } from "@components/Footer";
import { Header } from "@components/Header";
import { RootState } from "@src/redux/reducers";
import _ from "lodash";
import Head from "next/head";
import React from "react";
import { useSelector } from "react-redux";

type Props = {
    children: React.ReactNode;
    title: string;
    description?: string;
};

const Layout = ({ children, title, description }: Props) => {
    const { data } = useSelector((state: RootState) => state.settings);
    if (_.isEmpty(data)) {
        return null;
    }

    return (
        <>
            <Head>
                <title>{title + " | " + data?.SiteName}</title>
                {description && (
                    <meta name="description" content={description} />
                )}
            </Head>
            <Header data={data} />
            <main>{children}</main>
            <Footer data={data?.Footer} logo={data?.Logo} />
        </>
    );
};

export default Layout;
