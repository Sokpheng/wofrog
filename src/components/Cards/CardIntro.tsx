import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import { api } from "@src/config";
import Image from "next/image";
import React from "react";

type Props = {
    image: string | null | undefined;
    heading: string | null | undefined;
    description: string | null | undefined;
};

export const CardIntro = ({ image, heading, description }: Props) => {
    return (
        <Card
            elevation={3}
            sx={{
                borderRadius: (theme) => theme.shape.borderRadius,
                overflow: "hidden",
            }}
        >
            {image && (
                <CardMedia
                    sx={{
                        position: "relative",
                        aspectRatio: "16 / 9",
                    }}
                >
                    <Image
                        src={api.apiUrl + image}
                        layout="fill"
                        objectFit="contain"
                    />
                </CardMedia>
            )}

            <CardContent
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignContent: "center",
                    justifyContent: "center",
                }}
            >
                <Typography
                    variant="body1"
                    fontWeight="bold"
                    component="h2"
                    textAlign="center"
                    textTransform="uppercase"
                >
                    {heading}
                </Typography>
                <Typography variant="body1" textAlign="center" mt={2}>
                    {description}
                </Typography>
            </CardContent>
        </Card>
    );
};
