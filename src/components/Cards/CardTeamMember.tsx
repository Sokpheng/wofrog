import { Icon } from "@iconify/react";
import {
    Box,
    Button,
    Card,
    CardContent,
    CardMedia,
    Link,
    Stack,
    Typography,
} from "@mui/material";
import { api } from "@src/config";
import Image from "next/image";
import React from "react";
import NextLink from "next/link";

type Props = {
    image: string | null | undefined;
    name: string | null | undefined;
    description: string | null | undefined;
    callToAction: any;
};

const CardTeamMember = ({ image, description, callToAction, name }: Props) => {
    return (
        <Card
            elevation={0}
            sx={{
                minWidth: 250,
                maxWidth: 350,
                "&:hover": {
                    "& button": {
                        bgcolor: (theme) => theme.palette.primary.main,
                        color: (theme) => theme.palette.common.white,
                    },
                },
            }}
        >
            <CardMedia>
                <Box
                    sx={{
                        position: "relative",
                        aspectRatio: "16 / 9",
                        borderRadius: (theme) => theme.shape.borderRadius,
                        overflow: "hidden",
                    }}
                >
                    <Image
                        src={
                            image
                                ? api.apiUrl + image
                                : "/static/images/avatar.png"
                        }
                        layout="fill"
                        objectFit="cover"
                    />
                </Box>
                <CardContent>
                    <Stack textAlign="center" spacing={2}>
                        <Typography variant="h6" fontWeight="bold" textTransform="uppercase" mt={1} >
                            {name}
                        </Typography>
                        <Stack>
                            <Typography
                                variant="body1"
                                sx={{
                                    display: "-webkit-box",
                                    WebkitLineClamp: 3,
                                    WebkitBoxOrient: "vertical",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                }}
                            >
                                {description}
                            </Typography>
                        </Stack>
                        {callToAction && (
                            <Box>
                                <NextLink
                                    href={callToAction?.Link || "/"}
                                    passHref
                                >
                                    <Link underline="none" target="_blank" >
                                        <Button
                                            startIcon={
                                                <Icon
                                                    icon={
                                                        callToAction?.Icon || ""
                                                    }
                                                />
                                            }
                                            variant="outlined"
                                        >
                                            {callToAction?.Label}
                                        </Button>
                                    </Link>
                                </NextLink>
                            </Box>
                        )}
                    </Stack>
                </CardContent>
            </CardMedia>
        </Card>
    );
};

export default CardTeamMember;
