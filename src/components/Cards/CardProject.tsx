import { Icon } from "@iconify/react";
import {
    Box,
    Button,
    Card,
    CardContent,
    CardMedia,
    Link,
    Stack,
    Typography,
} from "@mui/material";
import { api } from "@src/config";
import Image from "next/image";
import React from "react";
import NextLink from "next/link";

type Props = {
    image: string | null | undefined;
    heading: string | null | undefined;
    description: string | null | undefined;
    callToAction: any;
};

const CardProject = ({ image, callToAction, description, heading }: Props) => {
    return (
        <Card
            elevation={3}
            sx={{
                borderRadius: (theme) => theme.shape.borderRadius,
                overflow: "hidden",
                "&:hover": {
                    "& .card-image": {
                        transform: "scale(1.02)",
                    },
                    "& .card-media": {
                        padding: 2,
                    },
                },
            }}
        >
            {image && (
                <CardMedia
                    className="card-media"
                    sx={{
                        bgcolor: (theme) => theme.palette.grey[100],
                        padding: 3,
                    }}
                >
                    <Box
                        className="card-image"
                        sx={{
                            position: "relative",
                            aspectRatio: "16 / 9",
                            borderRadius: (theme) => theme.shape.borderRadius,
                            overflow: "hidden",
                        }}
                    >
                        <Image
                            src={api.apiUrl + image}
                            layout="fill"
                            objectFit="cover"
                        />
                    </Box>
                </CardMedia>
            )}
            <CardContent>
                <Stack textAlign="center" spacing={2}>
                    <Typography variant="h6" component="h2">
                        {heading}
                    </Typography>
                    <Stack flexGrow={1}>
                        <Typography
                            variant="body1"
                            sx={{
                                display: "-webkit-box",
                                WebkitLineClamp: 3,
                                WebkitBoxOrient: "vertical",
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                            }}
                        >
                            {description}
                        </Typography>
                    </Stack>
                    <Box>
                        <NextLink href={callToAction?.Link || "#"} passHref>
                            <Link
                                underline="none"
                                target={
                                    callToAction?.Link ? "_blank" : "_parent"
                                }
                            >
                                <Button
                                    variant="outlined"
                                    startIcon={
                                        <Icon icon={callToAction?.Icon} />
                                    }
                                >
                                    {callToAction?.Label}
                                </Button>
                            </Link>
                        </NextLink>
                    </Box>
                </Stack>
            </CardContent>
        </Card>
    );
};

export default CardProject;
