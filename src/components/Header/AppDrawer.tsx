import {
    Box,
    Drawer,
    IconButton,
    Link,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Stack,
} from "@mui/material";
import React, { useState } from "react";
import AppLogo from "@components/Logo";
import NextLink from "next/link";
import { Icon } from "@iconify/react";

type Props = {
    logo: any;
    menus: any;
    open: boolean;
    onClose: () => void;
};

const AppDrawer = ({ logo, menus, open = false, onClose }: Props) => {
    return (
        <Drawer anchor="left" open={open} onClose={onClose}>
            <Stack minWidth={280} position="relative">
                <Box sx={{ position: "absolute", top: 4, right: 4 }}>
                    <IconButton onClick={onClose}>
                        <Icon icon="ic:twotone-keyboard-double-arrow-left" />
                    </IconButton>
                </Box>

                <Stack bgcolor={(theme) => theme.palette.grey[100]} p={4}>
                    <Box onClick={onClose}>
                        <AppLogo logo={logo} width="100%" />
                    </Box>
                </Stack>
                <List>
                    {menus?.data &&
                        menus?.data.map((menu: any) => (
                            <ListItem
                                // disableGutters
                                disablePadding
                                key={menu.id}
                                sx={{
                                    whiteSpace: "nowrap",
                                    textTransform: "capitalize",
                                }}
                            >
                                <NextLink
                                    href={`${menu?.attributes.Path || "/"}`}
                                    passHref
                                >
                                    <Link underline="none" flexGrow={1}>
                                        <ListItemButton>
                                            <ListItemIcon
                                                sx={{
                                                    color: (theme) =>
                                                        theme.palette.secondary
                                                            .main,
                                                }}
                                            >
                                                <Icon
                                                    icon={
                                                        menu?.attributes?.Icon
                                                    }
                                                    fontSize={24}
                                                />
                                            </ListItemIcon>
                                            <ListItemText
                                                primary={menu?.attributes.Name}
                                            />
                                        </ListItemButton>
                                    </Link>
                                </NextLink>
                            </ListItem>
                        ))}
                </List>
            </Stack>
        </Drawer>
    );
};

export default AppDrawer;
