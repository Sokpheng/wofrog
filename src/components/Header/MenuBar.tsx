import { api } from "@src/config";
import { RootState } from "@src/redux/reducers";
import Image from "next/image";
import NextLink from "next/link";
import React, { useCallback, useEffect, useState } from "react";
import _ from "lodash";
import {
    AppBar,
    Box,
    Container,
    Link,
    List,
    ListItem,
    ListItemText,
    Stack,
    Toolbar,
} from "@mui/material";
type Props = {
    menus: any;
    textColor?: string;
};

export const MenuBar = ({ menus, textColor }: Props) => {
    if (_.isEmpty(menus)) {
        return null;
    }

    return (
        <List sx={{ display: "flex", color: "red" }} disablePadding>
            {menus?.data &&
                menus?.data.map((menu: any) => (
                    <ListItem
                        key={menu.id}
                        sx={{
                            whiteSpace: "nowrap",
                            textTransform: "capitalize",
                        }}
                    >
                        <NextLink
                            href={`${
                                menu?.attributes?.IsHashRoute
                                    ? "#" + menu?.attributes.Path
                                    : menu?.attributes.Path || "/"
                            }`}
                            passHref
                        >
                            <Link underline="hover">
                                <ListItemText
                                    primary={menu?.attributes.Name}
                                    sx={{
                                        color: textColor
                                            ? textColor
                                            : "inherit",
                                    }}
                                />
                            </Link>
                        </NextLink>
                    </ListItem>
                ))}
        </List>
    );
};
