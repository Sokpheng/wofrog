import {
    AppBar,
    Box,
    Container,
    IconButton,
    Link,
    Stack,
    Toolbar,
    useMediaQuery,
    useTheme,
} from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import AppDrawer from "./AppDrawer";
import { MenuBar } from "./MenuBar";
import AppLogo from "@components/Logo";
import { Icon } from "@iconify/react";
import NextLink from "next/link";

type Props = {
    data: any;
};

export const Header = ({ data }: Props) => {
    const { Logo, Menus } = data;
    const [active, setActive] = useState(false);

    const [openDrawer, setOpenDrawer] = useState(false);

    const theme = useTheme();

    const isMatch = useMediaQuery(theme.breakpoints.down("md"));

    const handleScroll = useCallback(() => {
        if (window.scrollY > 100) {
            setActive(true);
        } else setActive(false);
    }, [setActive]);

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, []);

    return (
        <AppBar
            elevation={active ? 2 : 0}
            sx={{ bgcolor: active ? "white" : "transparent" }}
            position="sticky"
        >
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Stack
                        flexGrow={1}
                        direction="row"
                        alignItems="center"
                        // justifyContent="space-between"
                    >
                        {isMatch && (
                            <IconButton onClick={() => setOpenDrawer(true)}>
                                <Icon icon="ic:round-menu" />
                            </IconButton>
                        )}
                        <AppLogo logo={Logo} />
                        <Stack flexGrow={1} />
                        {isMatch ? (
                            <AppDrawer
                                logo={Logo}
                                menus={Menus}
                                open={openDrawer}
                                onClose={() => setOpenDrawer(false)}
                            />
                        ) : (
                            <MenuBar
                                menus={Menus}
                                textColor={!active ? "white" : "inherit"}
                            />
                        )}
                    </Stack>
                </Toolbar>
            </Container>
        </AppBar>
    );
};
