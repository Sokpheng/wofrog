import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const FooterLogoStyles = styled(Stack)(({ theme }) => ({
    display: "flex",
    flexDirection: "column",
    [theme.breakpoints.down("md")]: {
        justifyContent: "center",
        alignItems: "center",
    },
}));
