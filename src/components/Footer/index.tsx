import { api } from "@src/config";
import Image from "next/image";
import NextLink from "next/link";
import React from "react";
import { Icon } from "@iconify/react";
import _ from "lodash";
import {
    Container,
    Grid,
    IconButton,
    Link,
    List,
    ListItem,
    Stack,
    Typography,
} from "@mui/material";
import Logo from "@components/Logo";
import { FooterLogoStyles } from "./styles";

type Props = {
    data: any;
    logo?: any;
};

export const Footer = ({ data, logo }: Props) => {
    console.log("Footer: ", data);
    if (_.isEmpty(data)) {
        return null;
    }

    return (
        <Stack component="footer" py={5} >
            <Container maxWidth="xl">
                <Grid
                    container
                    justifyContent="center"
                    alignItems="center"
                    spacing={2}
                >
                    <Grid item xs={12} md={3}>
                        <FooterLogoStyles>
                            <Logo logo={logo} width={250} />
                            <Typography variant="caption" color="textSecondary">
                                {data?.Copyright}
                            </Typography>
                        </FooterLogoStyles>
                    </Grid>
                    <Grid item xs={12} md={7}>
                        <List
                            sx={{
                                display: "flex",
                                justifyContent: "center",
                            }}
                        >
                            {data?.FooterMenus?.data &&
                                data?.FooterMenus?.data.map((menu: any) => (
                                    <ListItem
                                        key={menu.id}
                                        sx={{
                                            width: "auto",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        <NextLink
                                            href={`${menu?.attributes.Path || "/"}`}
                                            passHref
                                        >
                                            <Link underline="none">
                                                {menu?.attributes?.Name}
                                            </Link>
                                        </NextLink>
                                    </ListItem>
                                ))}
                        </List>
                    </Grid>
                    <Grid item xs={12} md={1}>
                        <List
                            disablePadding
                            dense
                            sx={{
                                display: "flex",
                                justifyContent: "center",
                            }}
                        >
                            {data?.SocialMedia &&
                                data?.SocialMedia.map((item: any) => (
                                    <ListItem
                                        disableGutters
                                        key={item.id}
                                        sx={{
                                            width: "auto",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        <NextLink href={item?.Link || "/"} passHref>
                                            <Link underline="none">
                                                <IconButton>
                                                    <Icon
                                                        icon={item?.Icon || ""}
                                                    />
                                                </IconButton>
                                            </Link>
                                        </NextLink>
                                    </ListItem>
                                ))}
                        </List>
                    </Grid>
                </Grid>
            </Container>
        </Stack>
    );
};
