import { createSlice } from "@reduxjs/toolkit";
import Service from "@src/config/Service";

let initialState: any = {
    data: {},
};

const settingsSlice = createSlice({
    name: "settings",
    initialState,
    reducers: {
        setSettings: (state, action) => {
            state.data = action.payload?.attributes;
        },
    },
});

export const { setSettings } = settingsSlice.actions;

export default settingsSlice.reducer;

export const getSettings = () => async (dispatch: any) => {
    try {
        const resp = await Service.find("api/setting");
        dispatch(setSettings(resp.data?.data));
        return resp.data?.data;
    } catch (error) {
        console.log(error);
    }
};
