import { combineReducers } from "@reduxjs/toolkit";
import settings from "@redux/slices/settings";

const rootReducer = combineReducers({ settings });

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
