import axios from "axios";
import { api } from ".";

const API_URL = api.apiUrl;

const instance = axios.create({
    baseURL: API_URL,
    headers: {
        "Content-Type": "application/json",
    },
});

instance.defaults.headers.common["Content-Type"] = "application/json";

instance.interceptors.response.use(
    (response) => response.data,
    (error) => error.response.data,
);

const fineOne = (endpoint: string, id: string) => {
    return axios.get(API_URL + "/" + endpoint + "/" + id);
};

const find = (endpoint: string) => {
    console.log("process: ", process.env.NODE_ENV);
    console.log("url: ", API_URL );
    return axios.get(API_URL + "/" + endpoint);
};

export default {
    find,
    fineOne,
};
