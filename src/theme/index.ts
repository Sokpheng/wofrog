import { createTheme, responsiveFontSizes } from "@mui/material/styles";
import { TypographyOptions } from "@mui/material/styles/createTypography";
import { palette } from "./palette";
// declare module "@mui/material/styles" {
//     interface Palette {
//         tertiary: Palette["primary"];
//     }

//     // allow configuration using `createTheme`
//     interface PaletteOptions {
//         tertiary?: PaletteOptions["primary"];
//     }
// }

// // Update the Button's color prop options
// declare module "@mui/material/Button" {
//     interface ButtonPropsColorOverrides {
//         tertiary: true;
//     }
// }

// Create a theme instance.
let theme = createTheme({
    palette: palette,
});

theme = responsiveFontSizes(theme);

export default theme;
